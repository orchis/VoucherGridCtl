﻿using VoucherGridCtl.Commons;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static VoucherGridCtl.AccountSubjectPopup;
using Keyboard = VoucherGridCtl.Commons.Keyboard;

namespace VoucherGridCtl
{
    /// <summary>
    /// AccountSubjectBox.xaml 的交互逻辑
    /// </summary>
    public partial class AccountSubjectBox : TextBox
    {
        public AccountSubjectBox()
        {
            InitializeComponent();
            base.TextWrapping = TextWrapping.WrapWithOverflow;
            base.KeyDown += OnKeyDown;
            base.LostFocus += OnLostFocus;
            base.GotFocus += OnGotFocus;
            base.MouseDoubleClick += AccountSubjectBox_MouseDoubleClick;
        }

        private void AccountSubjectBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            AccountSubjectPopup popupForm = new AccountSubjectPopup();
            popupForm.SelectedEvent += OnSelected;
            popupForm.ShowDialog();
        }

        public AccountSubjectObj Value { get; private set; } = new AccountSubjectObj();


        public long Id
        {
            set
            {
                Value=AccountSubjectList.Find(value);
                base.Text = Value.FullName;
            }
            get { return Value.id; }
        }

        public string No
        {
            get
            {
                return Value.no;
            }            
        }

        public string FullName
        {
            get
            {
                return Value.FullName;
            }
        }

        private void OnGotFocus(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("OnGotFocus");
            if (base.IsReadOnly)
                return;
            base.Text = No;
            base.Background = Consts.HIGHLIGHT_BRUSH;
        }

        private void OnLostFocus(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("ACC OnLostFocus");
            if (base.IsReadOnly)
                return;
            FillAccountSubject();
            base.Background = Consts.WHITE_BRUSH;
        }

        private void OnSelected(SelectedEventArgs e)
        {
            this.Value = e.subjectObj;
            base.Text = No;
            e.Handled = true;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (base.IsReadOnly)
                return;
            if (e.Key == Key.Enter)
            {
                Keyboard.Press(Key.Tab);
            }
            else if(e.Key == Key.F7)
            {
                AccountSubjectPopup popupForm = new AccountSubjectPopup();
                popupForm.SelectedEvent += OnSelected;
                popupForm.ShowDialog();
            }
        }


        void FillAccountSubject()
        {
            var txt = base.Text;
            var obj = AccountSubjectList.FindByNo(txt.Trim());
            Value = obj;
            base.Text = obj.FullName;          
        }

    }
}
