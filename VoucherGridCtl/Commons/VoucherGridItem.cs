﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoucherGridCtl.Commons
{
    public class VoucherGridItem
    {
        public VoucherGridItem()
        {
            this.Content = string.Empty;            
            this.AccountSubject = new AccountSubjectObj();
            this.DebitsAmount = 0.00M;
            this.CreditAmount = 0.00M;
        }

        public string Content { set; get; }
        public long AccountSubjectId {
            set {
                AccountSubject.id = value;
                this.AccountSubject = AccountSubjectList.Find(value);
            }
            get {
                return AccountSubject.id;
            }
        }
        public AccountSubjectObj AccountSubject { set; get; }   
        /// <summary>
        /// 借方
        /// </summary>
        public decimal DebitsAmount { set; get; }
        /// <summary>
        /// 贷方
        /// </summary>
        public decimal CreditAmount { set; get; }
    }

    

    
}
