﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace VoucherGridCtl.Commons
{
    public static class Consts
    {
        internal static Brush BLACK_BRUSH = new SolidColorBrush(Colors.Black);
        internal static Brush WHITE_BRUSH = new SolidColorBrush(Colors.White);
        internal static Brush HIGHLIGHT_BRUSH = new SolidColorBrush(Colors.AliceBlue);

        //定义delegate
        public delegate List<AccountSubjectObj> GetAccountSubjectListEventHandler();
        public delegate List<AuxiliaryObj> GetAuxiliaryObjListEventHandler(AuxiliaryType type);
        
    }

    public enum FinanceAccountControlErrorCode
    {
        SUCCESS = 0,
        /// <summary>
        /// 取消
        /// </summary>
        CANCEL,
        /// <summary>
        /// 不存在
        /// </summary>
        NOT_EXIST,
        /// <summary>
        /// 不支持
        /// </summary>
        NOT_SUPPORT,
        /// <summary>
        /// 未知的系统错误
        /// </summary>
        SYSTEM_ERROR,
    }

    public class FinanceAccountControlException : Exception
    {
        string msg = "";
        public FinanceAccountControlException(FinanceAccountControlErrorCode ErrCode, string Message = "")
        {
            HResult = (int)ErrCode;
            msg = Message;
        }

        public override string Message
        {
            get
            {
                if (string.IsNullOrEmpty(msg))
                    return ((FinanceAccountControlErrorCode)HResult).ToString();
                else
                    return msg;
            }
        }
    }


}