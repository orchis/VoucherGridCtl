﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static VoucherGridCtl.Commons.Consts;

namespace VoucherGridCtl.Commons
{
    public enum AuxiliaryType
    {
        Invalid = 0,
        /// <summary>
        /// 科目组
        /// </summary>
        AccountGroup = 1,
        /// <summary>
        /// 现金流量项目
        /// </summary>
        CashFlowItem,
    }

    /// <summary>
    /// 缓存基础数据，由VoucherGrid对外暴露接口写入
    /// </summary>
    public class AuxiliaryList
    {
        //用event 关键字声明事件对象
        public event GetAuxiliaryObjListEventHandler GetAuxiliaryObjListEvent;

        static AuxiliaryList cache = null;
        public static AuxiliaryList Instance
        {
            get
            {
                if (cache == null)
                {
                    cache = new AuxiliaryList();
                }
                return cache;
            }
        }

        public static List<AuxiliaryObj> Get(AuxiliaryType type)
        {
            var list = Instance.AuxiliaryObjects(type);
            return list;
        }

        public static AuxiliaryObj Find(AuxiliaryType type, long accountSubjectId)
        {
            var accountSubjectObj = Get(type).FirstOrDefault(item => item.id == accountSubjectId);
            if (accountSubjectObj == null)
            {
                accountSubjectObj = new AuxiliaryObj();
            }
            return accountSubjectObj;
        }

        public static AuxiliaryObj FindByNo(AuxiliaryType type, string no)
        {
            var accountSubjectObj = Get(type).FirstOrDefault(item => item.no == no);
            if (accountSubjectObj == null)
            {
                accountSubjectObj = new AuxiliaryObj();
            }
            return accountSubjectObj;

        }

        List<AuxiliaryObj> AuxiliaryObjects(AuxiliaryType type)
        {
            var list = GetAuxiliaryObjListEvent?.Invoke(type);
            if (list == null)
                list = new List<AuxiliaryObj>();
            return list;
        }


    }
}
