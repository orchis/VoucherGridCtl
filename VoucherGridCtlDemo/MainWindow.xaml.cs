﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using VoucherGridCtl;
using VoucherGridCtl.Commons;

namespace VoucherGridCtlDemo
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AccountSubjectList.Instance.GetAccountSubjectListEvent += Instance_GetAccountSubjectListEvent;
            LoadVoucher();
        }

        private System.Collections.Generic.List<AccountSubjectObj> Instance_GetAccountSubjectListEvent()
        {
            return ListAccountSubjectObj;
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var txt = (sender as Button).Name;
                switch (txt)
                {
                    case "new":
                        voucherGrid.Clear();
                        voucherGrid.AppendRow();
                        break;
                    case "insert":
                        voucherGrid.InsertRow(voucherGrid.CurrentRowIndex);
                        break;
                    case "delete":
                        voucherGrid.DeleteRow(voucherGrid.CurrentRowIndex);
                        break;
                    case "save":
                        var lst = voucherGrid.DataSource;
                        var sb = new StringBuilder();
                        foreach (var item in lst)
                        {
                            if (item.AccountSubjectId == 0)
                                continue;
                            sb.AppendLine(string.Format("摘要：{0}    科目：{1}[{2}] 借方：{3} 贷方：{4}",
                                item.Content,
                                item.AccountSubject.no,
                                item.AccountSubject.name,
                                item.DebitsAmount,
                                item.CreditAmount));
                        }
                        FinanceMessageBox.Info(sb.ToString());
                        break;
                    case "load":
                        LoadVoucher();
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                FinanceMessageBox.Error(ex.Message);
            }
        }

        void LoadVoucher() {
            var items = new List<VoucherGridItem>();
            items.Add(new VoucherGridItem { Content = "取现金", AccountSubjectId = 1, DebitsAmount = 10 });
            items.Add(new VoucherGridItem { Content = "取现金", AccountSubjectId = 2, CreditAmount = 10 });
            voucherGrid.DataSource = items;
            voucherGrid.Refresh();
        }


        private List<AccountSubjectObj> ListAccountSubjectObj
        {
            get
            {
                var lst = new List<AccountSubjectObj>();
                lst.Add(new AccountSubjectObj
                {
                    id = 1,
                    no = "1001",
                    name = "库存现金",
                    parentId = 0,
                    rootId = 0,
                    groupId = 1,
                    level = 1,
                    isHasChild = false,
                    accountClass = AccountClass.Asset
                });
                lst.Add(new AccountSubjectObj
                {
                    id = 2,
                    no = "1002",
                    name = "银行存款",
                    parentId = 0,
                    rootId = 0,
                    groupId = 1,
                    level = 1,
                    isHasChild = false,
                    accountClass = AccountClass.Asset
                });
                lst.Add(new AccountSubjectObj
                {
                    id = 3,
                    no = "1012",
                    name = "其他货币资金",
                    parentId = 0,
                    rootId = 0,
                    groupId = 1,
                    level = 1,
                    isHasChild = true,
                    accountClass = AccountClass.Asset
                });
                lst.Add(new AccountSubjectObj
                {
                    id = 4,
                    no = "1012.01",
                    name = "银行汇票",
                    parentId = 3,
                    rootId = 3,
                    groupId = 1,
                    level = 2,
                    isHasChild = false,
                    accountClass = AccountClass.Asset
                });
                return lst;
            }
        }
    }
}
